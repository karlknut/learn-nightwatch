/*

[x] open delfi.ee
[x] pealkiri on "DELFI - Värsked uudised Eestist ja välismaalt - DELFI"
[x] vota screenshot
[x] page contains Podcast link
[] klikkides podcast lingi peale avaneb uus sait mille peakiri on "Delfi tasku"
[] vota screenshot

*/

const config = require('../../nightwatch.conf.js');

module.exports = {
  'Delfi Podcast': function bar(browser) {
    browser
      .url('https://www.delfi.ee/')
      .waitForElementVisible('body')
      .assert.title('DELFI - Värsked uudised Eestist ja välismaalt - DELFI')
      .saveScreenshot(`${config.imgpath(browser)}delfi-esileht.png`)
      .assert.containsText('.channel-categories', 'Podcastid')
      .useXpath()
      .assert.containsText("//a[text()='Podcastid']", 'Podcastid')
      .click("//a[text()='Podcastid']")
      .pause(500)
      .useCss()
      .waitForElementVisible('body')
      .assert.title('Delfi Tasku')
      .saveScreenshot(`${config.imgpath(browser)}delfi-tasku.png`)
      .end();
  },
};
