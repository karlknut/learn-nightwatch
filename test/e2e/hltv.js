/*

[x] open hltv.org
[x] title is "CS:GO News & Coverage | HLTV.org"
[x] take screenshot
[x] contains "Responsible use of your data"
[x] click "Allow all cookies"
[x] take screenshot
[x] check navbar contains "Events"
[x] click "Events"
[x] contains "Ongoing events"
[x] take screenshot
[x] click "ESL Pro League Season 13"
[x] take screenshot
[x] click "Stats"
[x] contains "Top players"
[x] contains "Top teams"
[x] contains "Top weapons"
[x] take screenshot

*/

const config = require('../../nightwatch.conf.js');

module.exports = {
  'HLTV Player Stats': function bar(browser) {
    browser
      .url('https://www.hltv.org/')
      .windowSize('current', 1000, 1000)
      .waitForElementVisible('body')
      .assert.title('CS:GO News & Coverage | HLTV.org')
      .saveScreenshot(`${config.imgpath(browser)}hltv-cookies.png`)
      .assert.containsText("//*[@id='CybotCookiebotDialogBodyContentTitle']", 'Responsible use of your data')
      .click("//*[@id='CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll']")
      .saveScreenshot(`${config.imgpath(browser)}hltv-main.png`)
      .assert.containsText("//a[text()='Events']", 'Events')
      .click("//a[text()='Events']")
      .assert.containsText("//h1[text()='Ongoing events']", 'Ongoing events')
      .saveScreenshot(`${config.imgpath(browser)}hltv-events.png`)
      .assert.containsText("//*[@id='FEATURED']/div/div/div/a[1]", 'ESL Pro League Season 13')
      .click("//*[@id='FEATURED']/div/div/div/a[1]")
      .saveScreenshot(`${config.imgpath(browser)}esl-pro-league-season-13.png`)
      .assert.containsText("//a[3][text()='Stats']", 'Stats')
      .click("//a[3][text()='Stats']")
      .assert.containsText("//div[text()='Top players']", 'Top players')
      .assert.containsText("//div[text()='Top teams']", 'Top teams')
      .assert.containsText("//div[text()='Top weapons']", 'Top weapons')
      .saveScreenshot(`${config.imgpath(browser)}tournament-stats.png`)
      .end();
  },
};
