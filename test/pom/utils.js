// utils.js

module.exports.bar = function barr(browser) {
  this.goToSite = function bar() {
    browser
      .windowMaximize()
      .url('http://testing.site.com')
      .waitForElementVisible('body', 1000);
    return browser;
  };
  this.mainLogo = function bar() {
    browser
      .click('body > nav > a');
    browser.assert.containsText('.style1>strong', 'Testing');
    return browser;
  };
  this.joinNow = function bar() {
    browser
      .click('#join-now-button');
  };
  this.newTestUser = function bar() {
    browser
      .click('#first-name')
      .setValue('#first-name', 'test')
      .setValue('#last-name', 'automation')
      .setValue('#email', 'testautomation@example.com')
      .setValue('#password', 'testing1');
  };
  this.logout = function bar() {
    browser
      .click('#customer_logout_link');
  };
  return this;
};
