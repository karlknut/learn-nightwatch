// tests.js
const utils = require('./utils');

module.exports = {
  '@tags': ['sampletest'],
  'Open the website': function bar(browser) {
    utils(browser).goToSite();
  },

  'Go to site and add user and logout': function bar(browser) {
    utils(browser).joinNow();
    utils(browser).newTestUser();
    utils(browser).mainLogo();
    browser.pause(5000);
    utils(browser).logout();
    browser.end();
  },
};
